# ConfigReader

## Zweck
Liest Verbindungsparameter aus einer INI-Datei. Die Werte
können dabei über Environment-Variablen übersteuert werden.

## Distribution / Download
- Siehe Package Repository: [Version 0.0.1](https://gitlab.com/hslu-i1/global/hslu_configreader/-/packages/6404326)

## Verwendung
- not yet

/*
 * Copyright 2022 Hochschule Luzern Informatik.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.hslu.configreader;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

/**
 * Testcases.
 */
class ConfigReaderTest {

    @Test
    void testResourceInClasspath() {
        final ConfigReader cr = new ConfigReader("demo");
        Assertions.assertAll(() -> {
            assertThat(cr.getHost()).isEqualTo("myhost");
            assertThat(cr.getPort()).isEqualTo(8080);
            assertThat(cr.getUsername()).isEqualTo("abc");
            assertThat(cr.getPassword()).isEqualTo("xxx");
            assertThat(cr.getExchange()).isEqualTo("myexchange");
        });
    }

    @Test
    void testResourceInFilesystem() {
        final ConfigReader cr = new ConfigReader("demo", "src/test/resources/file");
        Assertions.assertAll(() -> {
            assertThat(cr.getHost()).isEqualTo("myhostX");
            assertThat(cr.getPort()).isEqualTo(8080);
            assertThat(cr.getUsername()).isEqualTo("abcX");
            assertThat(cr.getPassword()).isEqualTo("xxxX");
            assertThat(cr.getExchange()).isEqualTo("myexchangeX");
        });
    }

    @Test
    void testUnknownValues() {
        final ConfigReader cr = new ConfigReader("wrong");
        Assertions.assertAll(() -> {
            assertThat(cr.getHost()).isEqualTo("unknown");
            assertThat(cr.getPort()).isEqualTo(0);
            assertThat(cr.getUsername()).isEqualTo("unknown");
            assertThat(cr.getPassword()).isEqualTo("unknown");
            assertThat(cr.getExchange()).isEqualTo("unknown");
        });
    }

    @Test
    void testBadValues() {
        final ConfigReader cr = new ConfigReader("baddata");
        Assertions.assertAll(() -> {
            assertThat(cr.getHost()).isEqualTo("");
            assertThat(cr.getPort()).isEqualTo(0);
            assertThat(cr.getUsername()).isEqualTo("");
            assertThat(cr.getPassword()).isEqualTo("");
            assertThat(cr.getExchange()).isEqualTo("");
        });
    }

}

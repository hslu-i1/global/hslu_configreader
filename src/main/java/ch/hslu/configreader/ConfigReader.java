/*
 * Copyright 2022 Roland Gisler, HSLU Informatik, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.hslu.configreader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

/**
 * Liest Verbindungsparameter aus Propertyfile.
 */
public final class ConfigReader {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigReader.class);

    private static final String FILE_EXT = ".properties";
    // deepcode ignore NonCryptoHardcodedSecret: no secret
    private static final String UNKNOWN = "unknown";

    private static final String HOST = "hostname";
    private static final String PORT = "port";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String EXCHANGE = "exchange";

    private final Properties classpathProps = new Properties();
    private final Properties filesystemProps = new Properties();
    private final String namespace;

    /**
     * Liest die Konfiguration vom Default-File ein.
     *
     * @param namespace Namensraum.
     */
    public ConfigReader(final String namespace) {
        this(namespace, ".");
    }

    /**
     * Liest die Konfiguration ein.
     *
     * @param namespace Namensraum.
     * @param basepath Dateiverzeichnis.
     */
    ConfigReader(final String namespace, final String basepath) {
        this.namespace = namespace.toUpperCase();
        final String fileName = this.namespace.toLowerCase() + FILE_EXT;
        try (final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileName)) {
            if (Objects.nonNull(inputStream)) {
                classpathProps.load(inputStream);
                LOG.debug("ConfigReader: Reading from resource file {}", fileName);
            }
        } catch (IOException e) {
            LOG.error("ConfigReader: Error reading from resource file {}", fileName);
        }
        
        final Path path = Paths.get(basepath, fileName);
        try (final InputStream inputStream = new FileInputStream(path.toString())) {
            filesystemProps.load(inputStream);
        } catch (IOException e) {
            LOG.debug("ConfigReader: File {} not found in filesystem, only for information", path);
        }
        
    }

    /**
     * @return Liefert das Attribut host.
     */
    public String getHost() {
        return this.getEnvOrFile(this.namespace + "_HOST", HOST, UNKNOWN);
    }

    /**
     * @return Liefert das Attribut host.
     */
    public int getPort() {
        int port = 0;
        final String value = this.getEnvOrFile(this.namespace + "_PORT", PORT, UNKNOWN);
        try {
            port = Integer.valueOf(value);
        } catch (NumberFormatException nfe) {
            LOG.error("ConfigReader: Invalid port number '{}'", value);
        }
        return port;
    }

    /**
     * @return Liefert das Attribut user.
     */
    public String getUsername() {
        return this.getEnvOrFile(this.namespace + "_USER", USER, UNKNOWN);
    }

    /**
     * @return Liefert das Attribut password.
     */
    public String getPassword() {
        return this.getEnvOrFile(this.namespace + "_PASSWORD", PASSWORD, UNKNOWN);
    }

    /**
     * @return Liefert das Attribut exchange.
     */
    public String getExchange() {
        return this.getEnvOrFile(this.namespace + "_EXCHANGE", EXCHANGE, UNKNOWN);
    }

    /**
     * Liest eine Setting aus Properties, und überschreibt es ggf. mit ENV.
     * @param envVar Name der Environmentvariable die vorrang hat.
     * @param propKey Name des Propertykeys.
     * @param defValue Default-Value, wenn kein Wert vorhanden.
     * @return Wert aus Env, Property oder Default.
     */
    private String getEnvOrFile(final String envVar, final String propKey, final String defValue) {
        String value = System.getenv(envVar);
        if (Objects.isNull(value)) {
            value = this.filesystemProps.getProperty(propKey, null);
        } else {
            LOG.debug("ConfigReader: Property {} overriden by environment", propKey);
        }
        if (Objects.isNull(value)) {
            value = this.classpathProps.getProperty(propKey, defValue);
        } else {
            LOG.debug("ConfigReader: Property {} overriden by filesystem source", propKey);
        }
        LOG.debug("ConfigReader: Property {} was read", propKey);
        return value;
    }
}
